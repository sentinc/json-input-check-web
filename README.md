# json-input-check-web [![NPM version][npm-image]][npm-url] [![Build Status][travis-image]][travis-url] [![Dependency Status][daviddm-image]][daviddm-url]
> 

## Installation

```sh
$ npm install --save json-input-check-web
```

## Usage

```js
var jsonInputCheckWeb = require('json-input-check-web');

jsonInputCheckWeb('Rainbow');
```
## License

Apache-2.0 © [Nikita Matusevich](http://matusevich.it)


[npm-image]: https://badge.fury.io/js/json-input-check-web.svg
[npm-url]: https://npmjs.org/package/json-input-check-web
[travis-image]: https://travis-ci.org/Black-Stork/json-input-check-web.svg?branch=master
[travis-url]: https://travis-ci.org/Black-Stork/json-input-check-web
[daviddm-image]: https://david-dm.org/Black-Stork/json-input-check-web.svg?theme=shields.io
[daviddm-url]: https://david-dm.org/Black-Stork/json-input-check-web
