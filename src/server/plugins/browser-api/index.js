"use strict";
const handlers = require("./handlers");

class BrowserAPI {
  constructor() {
    this.register = this.register.bind(this);
    this.register.attributes = {
      name: "BrowserAPI",
      version: "1.0.0"
    };
  }

  register(server, options, next) {
    server.route([
      {
        method: "GET",
        path: "/api/browser",
        handler: handlers.getBrowser
      }
    ]);
    next();
  }
}

export default new BrowserAPI();
