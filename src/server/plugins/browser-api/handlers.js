import BrowserUtils from "../../helpers/browser-utils";

export const getBrowser = (request, reply) => {
  const userAgent = request.headers["user-agent"];
  const browserUtils = new BrowserUtils(userAgent);

  return reply(browserUtils.detect());
};
