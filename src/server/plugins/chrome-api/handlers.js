import chromePropsAPI from "chrome-web-store-item-property";
import Boom from "boom";
import {WEB_STORES_IDS} from "../../constants/web-store.constants";

const releaseNotes = [
  {
    title: "Fixes",
    data: [
      "Check status icon is attached to the input field",
      "Check status icon was moved to the top-right corner",
      "Switch-button data is stored even after refreshing the page"
    ]
  },
  {
    title: "What's new",
    data: [
      "Textarea was added to the popup window.",
      "Switch-button data is stored for a website, not for a tab",
      "Style was changed and release notes was added to the popup"
    ]
  }
];

export const getDetails = (request, reply) => {
  return chromePropsAPI(WEB_STORES_IDS.CHROME)
    .then(data => {
      const response = data || {};
      response.releaseNotes = releaseNotes;
      return reply(response);
    })
    .catch(err => reply(Boom.badRequest(err)));
};
