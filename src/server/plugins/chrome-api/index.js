"use strict";
const handlers = require("./handlers");

class ChromeAPI {
  constructor() {
    this.register = this.register.bind(this);
    this.register.attributes = {
      name: "ChromeAPI",
      version: "1.0.0"
    };
  }

  register(server, options, next) {
    server.route([
      {
        method: "GET",
        path: "/api/chrome",
        handler: handlers.getDetails
      }
    ]);
    next();
  }
}

export default new ChromeAPI();
