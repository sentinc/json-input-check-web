import reducer from "../../client/reducers";

export default function initTop() {
  return {
    reducer,
    initialState: {
      chromeDetails: {
        loading: false,
        error: false,
        data: null
      },
      currentBrowser: {
        loading: false,
        error: false,
        data: null
      }
    }
  };
}
