import {
  BROWSER_TO_USERAGENT,
  BROWSER_TITLES,
  FIREFOX,
  OPERA,
  MS_IE,
  MS_EDGE,
  CHROME,
  SAFARI
} from "../constants/browser-constants";

class BrowserUtils {
  constructor(userAgent) {
    this.userAgent = userAgent;
  }

  detect() {
    const {userAgent} = this; // eslint-disable-line

    if (userAgent.indexOf(BROWSER_TO_USERAGENT.FIREFOX) > -1) {
        return FIREFOX;
    } else if (userAgent.indexOf(BROWSER_TO_USERAGENT.OPERA) > -1) {
        return OPERA;
    } else if (userAgent.indexOf(BROWSER_TO_USERAGENT.MS_IE) > -1) {
        return MS_IE;
    } else if (userAgent.indexOf(BROWSER_TO_USERAGENT.MS_EDGE) > -1) {
        return MS_EDGE;
    } else if (userAgent.indexOf(BROWSER_TO_USERAGENT.CHROME) > -1) {
        return CHROME;
    } else if (userAgent.indexOf(BROWSER_TO_USERAGENT.SAFARI) > -1) {
        return SAFARI;
    }

    return null;
  }

  isFirefox() {
    return this.detect() === FIREFOX;
  }

  isOpera() {
    return this.detect() === OPERA;
  }

  isInternetExplorer() {
    return this.detect() === MS_IE;
  }

  isEdge() {
    return this.detect() === MS_EDGE;
  }

  isChrome() {
    return this.detect() === CHROME;
  }

  isSafari() {
    return this.detect() === SAFARI;
  }

  getTitle(browserType) {
    return BROWSER_TITLES[browserType];
  }
}

export default BrowserUtils;
