export const CHROME = "CHROME";
export const FIREFOX = "FIREFOX";
export const MS_IE = "MS_IE";
export const MS_EDGE = "MS_EDGE";
export const SAFARI = "SAFARI";
export const OPERA = "OPERA";

export const BROWSER_TITLES = {
  [FIREFOX]: "Mozilla Firefox",
  [OPERA]: "Opera",
  [MS_IE]: "Internet Explorer",
  [MS_EDGE]: "Edge",
  [CHROME]: "Google Chrome",
  [SAFARI]: "Safari"
};

export const USERAGENT_TO_BROWSER = {
  ["Firefox"]: FIREFOX,
  ["Opera"]: OPERA,
  ["Trident"]: MS_IE,
  ["Edge"]: MS_EDGE,
  ["Chrome"]: CHROME,
  ["Safari"]: SAFARI
};

export const BROWSER_TO_USERAGENT = {
  [FIREFOX]: "Firefox",
  [OPERA]: "Opera",
  [MS_IE]: "Trident",
  [MS_EDGE]: "Edge",
  [CHROME]: "Chrome",
  [SAFARI]: "Safari"
};
