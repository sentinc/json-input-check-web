//
// This is the client side entry point for the React app.
//

import React from "react";
import { render, hydrate } from "react-dom";
import { routes } from "./routes";
import { BrowserRouter } from "react-router-dom";
import thunkMiddleware from "redux-thunk";
import { createStore, applyMiddleware } from "redux";
import { Provider } from "react-redux";
import rootReducer from "./reducers";
import { renderRoutes } from "react-router-config";
import "./styles/styles.css";

//

//
// Redux configure store with Hot Module Reload
//
// const configureStore = initialState => {
//   const store = createStore(rootReducer, initialState);

//   if (module.hot) {
//     module.hot.accept("./reducers", () => {
//       const nextRootReducer = require("./reducers").default;
//       store.replaceReducer(nextRootReducer);
//     });
//   }

//   return store;
// };

const createStoreWithMiddleware = applyMiddleware(thunkMiddleware)(createStore);
export const configureStore = (initialState) => {
  if (process.env.NODE_ENV !== "production") {
    return createStoreWithMiddleware(
      rootReducer,
      initialState,
      window.devToolsExtension ? window.devToolsExtension() : f => f
    );
  }

  return createStoreWithMiddleware(rootReducer, initialState);
};

const store = window.__PRELOADED_STATE__
  ? configureStore(window.__PRELOADED_STATE__)
  : configureStore({});

const start = App => {
  const jsContent = document.querySelector(".js-content");
  const reactStart = window.__PRELOADED_STATE__ && jsContent.innerHTML ? hydrate : render;

  reactStart(
    <Provider store={store}>
      <BrowserRouter>
        <App />
      </BrowserRouter>
    </Provider>,
    jsContent
  );
};

window.webappStart = () => start(() => renderRoutes(routes));

//
// Hot Module Reload setup
//
if (module.hot) {
  module.hot.accept("./routes", () => {
    const r = require("./routes");
    start(() => renderRoutes(r.routes));
  });
}
