import axios from "axios";

import {CHROME_FETCH_START, CHROME_FETCH_SUCCESS, CHROME_FETCH_ERROR,
  BROWSER_FETCH_START, BROWSER_FETCH_SUCCESS, BROWSER_FETCH_ERROR} from "./action-types";

export const fetchChrome = () => {
  return dispatch => {
    dispatch({
      type: CHROME_FETCH_START
    });

    return axios({
      method: "GET",
      responseType: "json",
      url: "api/chrome"
    }).then(response => {
      return dispatch({
        type: CHROME_FETCH_SUCCESS,
        data: response.data
      });
    }).catch((err => {
      return dispatch({
        type: CHROME_FETCH_ERROR,
        error: err
      });
    }));
  };
};

export const fetchBrowser = () => {
  return dispatch => {
    dispatch({
      type: BROWSER_FETCH_START
    });

    return axios({
      method: "GET",
      responseType: "text",
      url: "api/browser"
    }).then(response => {
      return dispatch({
        type: BROWSER_FETCH_SUCCESS,
        data: response.data
      });
    }).catch((err => {
      return dispatch({
        type: BROWSER_FETCH_ERROR,
        error: err
      });
    }));
  };
};
