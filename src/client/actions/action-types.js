const REQUEST = {
  START: "START",
  SUCCESS: "SUCCESS",
  ERROR: "ERROR"
};

const CHROME = "CHROME";
export const CHROME_FETCH_START = `${CHROME}.FETCH_${REQUEST.START}`;
export const CHROME_FETCH_SUCCESS = `${CHROME}.FETCH_${REQUEST.SUCCESS}`;
export const CHROME_FETCH_ERROR = `${CHROME}.FETCH_${REQUEST.ERROR}`;

const BROWSER = "BROWSER";
export const BROWSER_FETCH_START = `${BROWSER}.FETCH_${REQUEST.START}`;
export const BROWSER_FETCH_SUCCESS = `${BROWSER}.FETCH_${REQUEST.SUCCESS}`;
export const BROWSER_FETCH_ERROR = `${BROWSER}.FETCH_${REQUEST.ERROR}`;
