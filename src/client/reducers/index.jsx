import { combineReducers } from "redux";
import * as actionTypes from "../actions/action-types";

const chromeDetails = (store, action) => {
  switch (action.type) {
    case actionTypes.CHROME_FETCH_START:
      return {
        loading: true,
        error: false,
        data: null
      };
    case actionTypes.CHROME_FETCH_SUCCESS:
      return {
        loading: false,
        error: false,
        data: action.data
      };
    case actionTypes.CHROME_FETCH_ERROR:
      return {
        loading: false,
        error: action.error,
        data: null
      };
  }

  return store || {
    loading: false,
    error: false,
    data: null
  };
};

const currentBrowser = (store, action) => {
  switch (action.type) {
    case actionTypes.BROWSER_FETCH_START:
      return {
        loading: true,
        error: false,
        data: null
      };
    case actionTypes.BROWSER_FETCH_SUCCESS:
      return {
        loading: false,
        error: false,
        data: action.data
      };
    case actionTypes.BROWSER_FETCH_ERROR:
      return {
        loading: false,
        error: action.error,
        data: null
      };
  }

  return store || {
    loading: false,
    error: false,
    data: null
  };
};

export default combineReducers({
  chromeDetails,
  currentBrowser
});
