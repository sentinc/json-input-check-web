import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import PropTypes from "prop-types";

import classNames from "classnames";
import { Layout, Row } from "antd";
const { Content, Footer } = Layout;

import Logo from "./Logo";
import PlatformDetails from "./platformDetails";
import { fetchBrowser, fetchChrome } from "../actions";
import { CHROME, FIREFOX, SAFARI } from "../constants/browser-constants";

import imageLogo from "../images/logo_128.png";
import chromeLogo from "../images/google_chrome.svg";
import firefoxLogo from "../images/firefox.svg";
import safariLogo from "../images/safari.svg";

class Home extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    const {fetchChromeDetails, fetchBrowserDetails} = this.props;
    fetchBrowserDetails();
    fetchChromeDetails();
  }

  getChromeDetails() {
    const {chromeDetails} = this.props;

    if (!chromeDetails) {
      return {};
    }

    return {
      loading: chromeDetails.loading,
      error: chromeDetails.error,
      ...chromeDetails.data
    };
  }

  isCurrentBrowser(browserType) {
    const {currentBrowser} = this.props;

    return currentBrowser === browserType;
  }

  render() {
    const chromeDetails = this.getChromeDetails();

    return (
      <div className="mainBackground">
        <Layout className="mainLayout">
          <Content className="mainLayoutContent">
            <div className="mainLayoutContentBackground">
              <div className="mainLogoContainer">
                <Logo src={imageLogo} />
                <h1>JSON input-check</h1>
                <div className="descriptionContainer">
                  <h3 className="descriptionLabel">description</h3>
                  <p className="descriptionContent">
                    This extension is a JSON checker for input fields.
                  </p>
                </div>
                <Row className="platformsDetails">
                  <PlatformDetails
                    className={classNames({
                      activeBrowser: this.isCurrentBrowser(FIREFOX)
                    })}
                    available={false}
                    title="Firefox"
                    size={8}
                    icon={firefoxLogo}
                  />
                  <PlatformDetails
                    className={classNames({
                      activeBrowser: this.isCurrentBrowser(CHROME)
                    })}
                    title={chromeDetails.operatingSystem}
                    size={8}
                    icon={chromeLogo}
                    loading={chromeDetails.loading}
                    version={chromeDetails.version}
                    url={chromeDetails.url}
                    users={chromeDetails.interactionCount &&
                      chromeDetails.interactionCount.UserDownloads || 0}
                    releaseNotes={chromeDetails.releaseNotes}
                  />
                  <PlatformDetails
                    className={classNames({
                      activeBrowser: this.isCurrentBrowser(SAFARI)
                    })}
                    available={false}
                    title="Safari"
                    size={8}
                    icon={safariLogo}
                  />
                </Row>
              </div>
            </div>
          </Content>
          <Footer style={{ textAlign: "center" }}>
            Created by Nikita Matusevich © 2018
          </Footer>
        </Layout>
      </div>
    );
  }
}

Home.propTypes = {
  fetchBrowserDetails: PropTypes.func,
  currentBrowser: PropTypes.string,
  fetchChromeDetails: PropTypes.func,
  chromeDetails: PropTypes.object
};

const mapStateToProps = ({currentBrowser, chromeDetails}) => {
  return {
    currentBrowser: !currentBrowser.loading &&
      !currentBrowser.error && currentBrowser.data || "",
    chromeDetails
  };
};
const mapDispatchToProps = dispatch => ({
  fetchChromeDetails: bindActionCreators(fetchChrome, dispatch),
  fetchBrowserDetails: bindActionCreators(fetchBrowser, dispatch)
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Home);

