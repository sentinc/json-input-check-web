import React, { Component } from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";

export class Nav extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const currentTab = this.props.location.pathname.replace("/", "");
    return (
      <div className="navigation">
        <ul>
          <li styleName={currentTab === "" ? "active" : ""}>
            <Link to="/">Home</Link>
          </li>
          <li styleName={currentTab === "demo1" ? "active" : ""}>
            <Link to="/demo1">Demo1</Link>
          </li>
          <li styleName={currentTab === "demo2" ? "active" : ""}>
            <Link to="/demo2">Demo2</Link>
          </li>
        </ul>
        </div>
    );
  }
}

Nav.propTypes = {
  location: PropTypes.shape({
    pathname: PropTypes.string
  })
};

Nav.defaultProps = {
  location: {
    pathname: ""
  }
};
