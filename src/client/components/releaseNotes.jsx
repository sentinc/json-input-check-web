import * as React from "react";
import PropTypes from "prop-types";
import cx  from "classnames";

import { Collapse, List, Row } from "antd";

const ReleaseNotes = ({className, releaseNotes}) => {
  const renderItem = (line) => (
    <List.Item>{line}</List.Item>
  );

  return (
    <Row className={cx(className, "releaseNotes")}>
      <Collapse bordered={false}>
      {
        releaseNotes && releaseNotes.map((item, key) => (
          item && item.data && item.data.length > 0 && (
            <Collapse.Panel
              className="releaseNotesPanel"
              header={item.title}
              key={`${key}`}
            >
              <List
                size="small"
                bordered={true}
                dataSource={item.data}
                renderItem={renderItem}
              />
            </Collapse.Panel>
          )
        ))
      }
      </Collapse>
    </Row>
  );
};

ReleaseNotes.propTypes = {
  releaseNotes: PropTypes.arrayOf(
    PropTypes.shape({
      title: PropTypes.string,
      data: PropTypes.arrayOf(
        PropTypes.string
      )
    })
  ),
  className: PropTypes.string
};

export default ReleaseNotes;
