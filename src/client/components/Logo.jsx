import React from "react";
import PropTypes from "prop-types";

const Logo = ({height, width, src}) => {

  return (
    <div
      className="logoMainContainer"
      style={{
        height,
        width,
        backgroundImage: `url(${src})`
      }}
    />
  );
};

Logo.propTypes = {
  src: PropTypes.string,
  height: PropTypes.number,
  width: PropTypes.number
};

Logo.defaultProps = {
  src: "",
  height: 80,
  width: 80
};

export default Logo;
