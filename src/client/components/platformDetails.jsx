import React from "react";
import PropTypes from "prop-types";

import {Spin, Card, Col, Icon, Badge} from "antd";
const {Meta} = Card;

import ReleaseNotes from "./releaseNotes";

export const PlatformProperty = ({title, content}) => {
  return (
    <div className="propertyContainer">
      <h3 className="propertyTitle">{title}</h3>
      <p className="propertyContent">{content}</p>
    </div>
  );
};
PlatformProperty.propTypes = {
  title: PropTypes.string.isRequired,
  content: PropTypes.node.isRequired
};

const PlatformDetails = ({
  className,
  size,
  available,
  loading,
  title,
  icon,
  version,
  url,
  users,
  releaseNotes
}) => {
  const properties = [];
  if (version) {
    properties.push({
      title: "version",
      content: version
    });
  }
  if (url) {
    properties.push({
      title: "url",
      content: (
        <Icon
          type="link"
          theme="outlined"
          onClick={() => window.open(url)}
        />
      )
    });
  }

  if (users) {
    properties.push({
      title: "users",
      content: users
    });
  }

  return (
    <Col className="platformDetails" span={size}>
      <Spin spinning={loading}>
      {
        className && (
          <Badge
            count={<Icon type="check" theme="outlined" />}
            style={{ backgroundColor: "#09c49c" }}
          />
        )
      }
      <Card
        className={className}
        cover={
          <img
            className="platformLogo"
            alt={title}
            src={icon}
          />
        }
        actions={
          available
            ? properties.map((property, key) => (
              <PlatformProperty
                key={key}
                title={property.title}
                content={property.content}
              />
            ))
            : null
        }
      >
        <Meta
          title={title}
          description={
            available
              ? <ReleaseNotes
                  className="releaseNotes"
                  releaseNotes={releaseNotes}
                />
              : <h3>
                  Coming soon.. <Icon type="frown" theme="outlined" />
                </h3>
          }
          />
        </Card>
      </Spin>
    </Col>
  );
};

PlatformDetails.defaultProps = {
  available: true
};

PlatformDetails.propTypes = {
  available: PropTypes.bool,
  loading: PropTypes.bool,
  error: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.bool
  ]),
  title: PropTypes.string,
  icon: PropTypes.string,
  size: PropTypes.number,
  version: PropTypes.string,
  url: PropTypes.string,
  users: PropTypes.number,
  releaseNotes: PropTypes.array,
  className: PropTypes.string
};

export default PlatformDetails;
